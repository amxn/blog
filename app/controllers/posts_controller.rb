class PostsController < ApplicationController

  http_basic_authenticate_with name: 'ameen', password: 'qweqweqwe', except: [:index, :show]

  def index
    @posts = Post.all

    respond_to do |format|
      format.html  # index.html.erb
      format.json  { render :json => @posts }
    end
  end

  def new
    @post = Post.new
  end
  
  def show
    @post = Post.find(params[:id])
  end

  def create
    # render text: params[:post].inspect
    # @post = Post.new(post_params.permit(:title, :text))
    @post = Post.new(params[:post])
    
    if @post.save
      redirect_to @post
    else
      render 'new'
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])

    if @post.update_attributes(params[:post])
      redirect_to post_path
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    
    redirect_to posts_path
  end


  # private

  #   def post_params
  #     params.require(:post).permit(:title, :text)
  #   end

end
